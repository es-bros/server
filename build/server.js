'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _app = require('./src/app');

var _app2 = _interopRequireDefault(_app);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function startApp(id) {
	var port = process.env.PORT || 4000;
	(0, _app2.default)().listen(port, function () {
		return console.log('App started successfully!');
	});
}

exports.default = startApp;