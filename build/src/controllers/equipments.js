'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.EquipmentsController = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _util = require('util');

var _util2 = _interopRequireDefault(_util);

var _database = require('../config/database');

var _database2 = _interopRequireDefault(_database);

var _EquipmentDao = require('../persistence/EquipmentDao');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var EquipmentsController = exports.EquipmentsController = function () {
	function EquipmentsController() {
		_classCallCheck(this, EquipmentsController);
	}

	_createClass(EquipmentsController, [{
		key: 'getAll',
		value: function getAll(req, res) {
			var connection = (0, _database2.default)();
			var equipmentDao = new _EquipmentDao.EquipmentDao(connection);

			equipmentDao.getAll(function (err, rows) {
				if (err) return res.status(400).send(err);

				res.send(rows);
			});

			return connection.end();
		}
	}, {
		key: 'findById',
		value: function findById(req, res) {
			req.checkParams('id', 'Id is empty').notEmpty().withMessage('Must be an integer').isInt();

			req.getValidationResult().then(function (result) {
				if (!result.isEmpty()) return res.status(400).send('There have been validation errors: ' + _util2.default.inspect(result.array()));

				var connection = (0, _database2.default)();
				var equipmentDao = new _EquipmentDao.EquipmentDao(connection);

				var equipmentId = req.params.id;

				equipmentDao.findById(equipmentId, function (err, rows) {
					if (err) return res.status(400).send(err);

					if (rows.length == 0) return res.sendStatus(404);

					res.send(rows[0]);
				});

				return connection.end();
			});
		}
	}, {
		key: 'post',
		value: function post(req, res) {
			req.checkBody('name', 'Name is empty').notEmpty();
			req.checkBody('description', 'Description is empty').notEmpty();
			req.checkBody('fk_place_id', 'Place ID is empty').notEmpty().withMessage('Must be an integer').isInt();
			req.checkBody('fk_responsible_id', 'Responsible ID is empty').notEmpty().withMessage('Must be an integer').isInt();

			req.getValidationResult().then(function (result) {
				if (!result.isEmpty()) return res.status(400).send('There have been validation errors: ' + _util2.default.inspect(result.array()));

				var connection = (0, _database2.default)();
				var equipmentDao = new _EquipmentDao.EquipmentDao(connection);

				req.sanitizeBody('name').trim();
				req.sanitizeBody('description').trim();

				var equipment = req.body;

				equipmentDao.save(equipment, function (err, row) {
					if (err) return res.status(400).send(err);

					equipment.id = row.insertId;

					var response = {
						'equipment': equipment,
						links: [{
							href: req.originalUrl + equipment.id,
							rel: "read",
							method: "GET"
						}, {
							href: req.originalUrl + equipment.id,
							rel: "update",
							method: "PUT"
						}, {
							href: req.originalUrl + equipment.id,
							rel: "delete",
							method: "DELETE"
						}]
					};

					res.location(req.originalUrl + equipment.id);
					res.status(201).send(response);
				});

				return connection.end();
			});
		}
	}, {
		key: 'put',
		value: function put(req, res) {
			req.checkParams('id', 'Id is empty').notEmpty().withMessage('Must be an integer').isInt();

			req.getValidationResult().then(function (result) {
				if (!result.isEmpty()) return res.status(400).send('There have been validation errors: ' + _util2.default.inspect(result.array()));

				var connection = (0, _database2.default)();
				var equipmentDao = new _EquipmentDao.EquipmentDao(connection);

				var equipment = req.body;
				var equipmentId = req.params.id;
				equipment.id = equipmentId;

				equipmentDao.update(equipment, function (err, row) {
					if (err) return res.status(400).send(err);

					if (row.affectedRows == 0) return res.sendStatus(404);

					res.sendStatus(200);
				});

				return connection.end();
			});
		}
	}, {
		key: 'delete',
		value: function _delete(req, res) {
			req.checkParams('id', 'Id is empty').notEmpty().withMessage('Must be an integer').isInt();

			req.getValidationResult().then(function (result) {
				if (!result.isEmpty()) return res.status(400).send('There have been validation errors: ' + _util2.default.inspect(result.array()));

				var connection = (0, _database2.default)();
				var equipmentDao = new _EquipmentDao.EquipmentDao(connection);

				var equipmentId = req.params.id;

				equipmentDao.remove(equipmentId, function (err, result) {
					if (err) return res.status(400).send(err);

					res.sendStatus(204);
				});

				return connection.end();
			});
		}
	}]);

	return EquipmentsController;
}();