'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.PlacesController = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _util = require('util');

var _util2 = _interopRequireDefault(_util);

var _database = require('../config/database');

var _database2 = _interopRequireDefault(_database);

var _PlaceDao = require('../persistence/PlaceDao');

var _EquipmentDao = require('../persistence/EquipmentDao');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var PlacesController = exports.PlacesController = function () {
	function PlacesController() {
		_classCallCheck(this, PlacesController);
	}

	_createClass(PlacesController, [{
		key: 'getAll',
		value: function getAll(req, res) {
			var connection = (0, _database2.default)();

			var placeDao = new _PlaceDao.PlaceDao(connection);

			placeDao.getAll(function (err, rows) {
				if (err) return res.status(400).send(err);

				res.send(rows);
			});

			return connection.end();
		}
	}, {
		key: 'findById',
		value: function findById(req, res) {
			req.checkParams('id', 'Id is empty').notEmpty().withMessage('Must be an integer').isInt();

			req.getValidationResult().then(function (result) {
				if (!result.isEmpty()) return res.status(400).send('There have been validation errors: ' + _util2.default.inspect(result.array()));

				var connection = (0, _database2.default)();
				var placeDao = new _PlaceDao.PlaceDao(connection);

				var placeId = req.params.id;

				placeDao.findById(placeId, function (err, rows) {
					if (err) return res.sendStatus(400);

					if (rows.length == 0) return res.sendStatus(404);

					res.send(rows[0]);
				});

				return connection.end();
			});
		}
	}, {
		key: 'getEquipments',
		value: function getEquipments(req, res) {
			req.checkParams('id', 'Id is empty').notEmpty().withMessage('Must be an integer').isInt();

			req.getValidationResult().then(function (result) {
				if (!result.isEmpty()) return res.status(400).send('There have been validation errors: ' + _util2.default.inspect(result.array()));

				var connection = (0, _database2.default)();
				var equipmentDao = new _EquipmentDao.EquipmentDao(connection);

				var placeId = req.params.id;

				equipmentDao.findByPlaceId(placeId, function (err, rows) {
					if (err) return res.status(400).send(err);

					res.send(rows);
				});

				return connection.end();
			});
		}
	}, {
		key: 'post',
		value: function post(req, res) {
			req.checkBody('name', 'Name is empty').notEmpty();
			req.checkBody('latitude', 'Latitude is empty').notEmpty();
			req.checkBody('longitude', 'Longitude is empty').notEmpty();

			req.getValidationResult().then(function (result) {
				if (!result.isEmpty()) return res.status(400).send('There have been validation errors: ' + _util2.default.inspect(result.array()));

				var connection = (0, _database2.default)();
				var placeDao = new _PlaceDao.PlaceDao(connection);

				req.sanitizeBody('name').trim();
				req.sanitizeBody('description').trim();
				var place = req.body;

				placeDao.save(place, function (err, row) {
					if (err) return res.status(400).send(err);

					place.id = row.insertId;

					var response = {
						'place': place,
						links: [{
							href: req.originalUrl + place.id,
							rel: "read",
							method: "GET"
						}, {
							href: req.originalUrl + place.id,
							rel: "update",
							method: "PUT"
						}, {
							href: req.originalUrl + place.id,
							rel: "delete",
							method: "DELETE"
						}]
					};

					res.location(req.originalUrl + place.id);
					res.status(201).send(response);
				});

				return connection.end();
			});
		}
	}, {
		key: 'put',
		value: function put(req, res) {
			req.checkParams('id', 'Id is empty').notEmpty().withMessage('Must be an integer').isInt();

			req.getValidationResult().then(function (result) {
				if (!result.isEmpty()) return res.status(400).send('There have been validation errors: ' + _util2.default.inspect(result.array()));

				var connection = (0, _database2.default)();
				var placeDao = new _PlaceDao.PlaceDao(connection);

				var place = req.body;
				var placeId = req.params.id;
				place.id = placeId;

				placeDao.update(place, function (err, row) {
					if (err) return res.status(400).send(err);

					if (row.affectedRows == 0) return res.sendStatus(404);

					res.sendStatus(200);
				});

				return connection.end();
			});
		}
	}, {
		key: 'delete',
		value: function _delete(req, res) {
			req.checkParams('id', 'Id is empty').notEmpty().withMessage('Must be an integer').isInt();

			req.getValidationResult().then(function (result) {
				if (!result.isEmpty()) return res.status(400).send('There have been validation errors: ' + _util2.default.inspect(result.array()));

				var connection = (0, _database2.default)();
				var placeDao = new _PlaceDao.PlaceDao(connection);

				var placeId = req.params.id;

				placeDao.remove(placeId, function (err, row) {
					if (err) return res.status(400).send(err);
					res.sendStatus(204);
				});

				return connection.end();
			});
		}
	}]);

	return PlacesController;
}();