'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.UsersController = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _util = require('util');

var _util2 = _interopRequireDefault(_util);

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _database = require('../config/database');

var _database2 = _interopRequireDefault(_database);

var _config = require('../config/config');

var _config2 = _interopRequireDefault(_config);

var _UserDao = require('../persistence/UserDao');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var UsersController = exports.UsersController = function () {
	function UsersController() {
		_classCallCheck(this, UsersController);
	}

	_createClass(UsersController, [{
		key: 'get',
		value: function get(req, res) {
			res.send({ msg: "Token autenticado com sucesso!" });
		}

		//Ajustar async para fechar conexão no final.

	}, {
		key: 'token',
		value: function token(req, res) {
			req.checkBody('email', 'Invalid email').notEmpty().isEmail();
			req.checkBody('password', 'Invalid password').notEmpty();

			req.getValidationResult().then(function (result) {
				if (!result.isEmpty()) return res.status(400).send('There have been validation errors: ' + _util2.default.inspect(result.array()));

				var connection = (0, _database2.default)();
				var userDao = new _UserDao.UserDao(connection);

				userDao.findByEmail(req.body.email, function (err, rows) {
					if (err) return res.sendStatus(400);

					if (rows.length == 0) return res.sendStatus(404);

					var user = rows[0];

					if (user.password == req.body.password) {
						var payload = { email: user.email };
						var token = _jsonwebtoken2.default.sign(payload, _config2.default.security.jwtSecret, { expiresIn: '7d' });
						user.access_token = token;
						userDao.updateToken(user, function (err, row) {
							if (err) return res.status(400).send(err);

							if (row.affectedRows == 0) return res.sendStatus(404);

							res.send({ token: token });
						});
					}
				});
			});
		}
	}]);

	return UsersController;
}();