'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.VersionController = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _async = require('async');

var _async2 = _interopRequireDefault(_async);

var _database = require('../config/database');

var _database2 = _interopRequireDefault(_database);

var _VersionDao = require('../persistence/VersionDao');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var VersionController = exports.VersionController = function () {
	function VersionController() {
		_classCallCheck(this, VersionController);
	}

	_createClass(VersionController, [{
		key: 'get',
		value: function get(req, res) {
			var connection = (0, _database2.default)();
			var versionDao = new _VersionDao.VersionDao(connection);

			versionDao.get(function (err, rows) {
				if (err || rows.length == 0) return res.status(400).send(err);

				res.send(rows[0]);
			});

			return connection.end();
		}
	}, {
		key: 'increment',
		value: function increment(req, res) {
			var connection = (0, _database2.default)();
			var versionDao = new _VersionDao.VersionDao(connection);

			_async2.default.waterfall([function (callback) {
				versionDao.get(function (err, rows) {
					var currentVersion = rows[0].current;
					callback(err, currentVersion, versionDao);
				});
			}, function (currentVersion, callback) {
				var newVersion = currentVersion++;
				versionDao.update(newVersion, new Date(), function (err, rows) {
					callback(err, newVersion);
				});
			}], function (err, newVersion) {
				if (err) {
					res.status(400).send(err);
				} else {
					res.send({ current: newVersion });
				}
				return connection.end();
			});
		}
	}]);

	return VersionController;
}();