'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.ResponsiblesController = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _util = require('util');

var _util2 = _interopRequireDefault(_util);

var _database = require('../config/database');

var _database2 = _interopRequireDefault(_database);

var _ResponsibleDao = require('../persistence/ResponsibleDao');

var _EquipmentDao = require('../persistence/EquipmentDao');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ResponsiblesController = exports.ResponsiblesController = function () {
	function ResponsiblesController() {
		_classCallCheck(this, ResponsiblesController);
	}

	_createClass(ResponsiblesController, [{
		key: 'getAll',
		value: function getAll(req, res) {
			var connection = (0, _database2.default)();
			var responsibleDao = new _ResponsibleDao.ResponsibleDao(connection);

			responsibleDao.getAll(function (err, rows) {
				if (err) return res.status(400).send(err);

				res.send(rows);
			});

			return connection.end();
		}
	}, {
		key: 'findById',
		value: function findById(req, res) {
			req.checkParams('id', 'Id is empty').notEmpty().withMessage('Must be an integer').isInt();

			req.getValidationResult().then(function (result) {
				if (!result.isEmpty()) return res.status(400).send('There have been validation errors: ' + _util2.default.inspect(result.array()));

				var connection = (0, _database2.default)();
				var responsibleDao = new _ResponsibleDao.ResponsibleDao(connection);

				var responsibleId = req.params.id;

				responsibleDao.findById(responsibleId, function (err, row) {
					if (err) return res.status(400).send(err);

					if (row.length == 0) return res.sendStatus(404);

					res.send(row[0]);
				});

				return connection.end();
			});
		}
	}, {
		key: 'post',
		value: function post(req, res) {
			req.checkBody('name', 'Name is empty').notEmpty();

			req.checkBody('email', 'E-mail is empty').notEmpty().withMessage('Valid e-mail required').isEmail();

			req.checkBody('phone', 'Phone is empty').notEmpty();

			req.getValidationResult().then(function (result) {
				if (!result.isEmpty()) return res.status(400).send('There have been validation errors: ' + _util2.default.inspect(result.array()));

				var connection = (0, _database2.default)();
				var responsibleDao = new _ResponsibleDao.ResponsibleDao(connection);

				req.sanitizeBody('name').trim();
				req.sanitizeBody('email').trim();
				req.sanitizeBody('phone').trim();

				var responsible = req.body;

				responsibleDao.save(responsible, function (err, row) {
					if (err) return res.status(400).send(err);

					responsible.id = row.insertId;

					var response = {
						'responsible': responsible,
						links: [{
							href: req.originalUrl + responsible.id,
							rel: "read",
							method: "GET"
						}, {
							href: req.originalUrl + responsible.id,
							rel: "update",
							method: "PUT"
						}, {
							href: req.originalUrl + responsible.id,
							rel: "delete",
							method: "DELETE"
						}]
					};

					res.location(req.originalUrl + responsible.id);
					res.status(201).send(response);
				});

				return connection.end();
			});
		}
	}, {
		key: 'put',
		value: function put(req, res) {
			req.checkParams('id', 'Id is empty').notEmpty().withMessage('Must be an integer').isInt();

			req.getValidationResult().then(function (result) {
				if (!result.isEmpty()) return res.status(400).send('There have been validation errors: ' + _util2.default.inspect(result.array()));

				var connection = (0, _database2.default)();
				var responsibleDao = new _ResponsibleDao.ResponsibleDao(connection);

				var responsible = req.body;
				var responsibleId = req.params.id;
				responsible.id = responsibleId;

				responsibleDao.update(responsible, function (err, row) {
					if (err) return res.status(400).send(err);

					if (row.affectedRows == 0) return res.sendStatus(404);

					res.sendStatus(200);
				});

				return connection.end();
			});
		}
	}, {
		key: 'delete',
		value: function _delete(req, res) {
			req.checkParams('id', 'Id is empty').notEmpty().withMessage('Must be an integer').isInt();

			req.getValidationResult().then(function (result) {
				if (!result.isEmpty()) return res.status(400).send('There have been validation errors: ' + _util2.default.inspect(result.array()));

				var connection = (0, _database2.default)();
				var responsibleDao = new _ResponsibleDao.ResponsibleDao(connection);

				var responsibleId = req.params.id;

				responsibleDao.remove(responsibleId, function (err, rows) {
					if (err) return res.status(400).send(err);

					res.sendStatus(204);
				});

				return connection.end();
			});
		}
	}, {
		key: 'getEquipments',
		value: function getEquipments(req, res) {
			req.checkParams('id', 'Id is empty').notEmpty().withMessage('Must be an integer').isInt();

			req.getValidationResult().then(function (result) {
				if (!result.isEmpty()) return res.status(400).send('There have been validation errors: ' + _util2.default.inspect(result.array()));

				var connection = (0, _database2.default)();
				var equipmentDao = new _EquipmentDao.EquipmentDao(connection);

				var responsibleId = req.params.id;

				equipmentDao.findByResponsibleId(responsibleId, function (err, rows) {
					if (err) return res.status(400).send(err);

					res.send(rows);
				});

				return connection.end();
			});
		}
	}]);

	return ResponsiblesController;
}();