'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.MobileController = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _async = require('async');

var _async2 = _interopRequireDefault(_async);

var _database = require('../config/database');

var _database2 = _interopRequireDefault(_database);

var _VersionDao = require('../persistence/VersionDao');

var _EquipmentDao = require('../persistence/EquipmentDao');

var _PlaceDao = require('../persistence/PlaceDao');

var _ResponsibleDao = require('../persistence/ResponsibleDao');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MobileController = exports.MobileController = function () {
	function MobileController() {
		_classCallCheck(this, MobileController);
	}

	_createClass(MobileController, [{
		key: 'get',
		value: function get(req, res) {

			var clientVersion = parseInt(req.query.version);

			var connection = (0, _database2.default)();
			var versionDao = new _VersionDao.VersionDao(connection);

			_async2.default.waterfall([function (callback) {
				versionDao.get(function (err, rows) {
					var databaseVersion = rows[0].current;
					callback(err, databaseVersion);
				});
			}, function (databaseVersion, callback) {
				clientVersion < databaseVersion ? callback(null, databaseVersion) : callback({ msg: "Your database is up to date!" });
			}, function (databaseVersion, callback) {
				_getAllDatabase(databaseVersion, connection, callback);
			}], function (err, results) {
				if (err) {
					res.status(400).send(err);
				} else {
					res.send(results);
				}

				return connection.end();
			});
		}
	}, {
		key: '_getAllDatabase',
		value: function _getAllDatabase(databaseVersion, connection, cb) {

			var equipmentDao = new _EquipmentDao.EquipmentDao(connection);
			var placeDao = new _PlaceDao.PlaceDao(connection);
			var responsibleDao = new _ResponsibleDao.ResponsibleDao(connection);

			_async2.default.parallel({
				places: function places(callback) {
					placeDao.getAll(function (err, places) {
						callback(err, places);
					});
				},
				equipments: function equipments(callback) {
					equipmentDao.getAll(function (err, equipments) {
						callback(err, equipments);
					});
				},
				responsibles: function responsibles(callback) {
					responsibleDao.getAll(function (err, responsibles) {
						callback(err, responsibles);
					});
				}
			}, function (err, results) {
				if (err) return cb(err);

				results.version = databaseVersion;
				return cb(null, results);
			});
		}
	}]);

	return MobileController;
}();