'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _passportJwt = require('passport-jwt');

var _passportJwt2 = _interopRequireDefault(_passportJwt);

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _UserDao = require('./persistence/UserDao');

var _config = require('./config/config');

var _config2 = _interopRequireDefault(_config);

var _database = require('./config/database');

var _database2 = _interopRequireDefault(_database);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ExtractJwt = _passportJwt2.default.ExtractJwt;
var Strategy = _passportJwt2.default.Strategy;

var params = {
	secretOrKey: _config2.default.security.jwtSecret,
	jwtFromRequest: ExtractJwt.fromAuthHeader()
};

exports.default = function () {

	var strategy = new Strategy(params, function (payload, done) {

		var connection = (0, _database2.default)();
		var userDao = new _UserDao.UserDao(connection);

		userDao.findByEmail(payload.email, function (err, rows) {
			if (err || rows.length == 0) return done(new Error("User not found"), false);

			var user = rows[0];

			if (user) {
				var userToken = _jsonwebtoken2.default.decode(user.access_token);

				if (userToken.email == payload.email && userToken.iat == payload.iat && userToken.exp == payload.exp) {
					return done(null, user);
				}
			}

			return done(new Error("User or token invalid"), false);
		});

		return connection.end();
	});

	_passport2.default.use(strategy);

	return {
		initialize: function initialize() {
			return _passport2.default.initialize();
		},
		authenticate: function authenticate() {
			return _passport2.default.authenticate("jwt", _config2.default.security.jwtSession);
		}
	};
};