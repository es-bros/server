'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _mysql = require('mysql');

var _mysql2 = _interopRequireDefault(_mysql);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createDBConnection = function createDBConnection() {

	if (!process.env.NODE_ENV || process.env.node === 'dev') {
		return _mysql2.default.createConnection({
			host: 'localhost',
			user: 'almada',
			password: '123',
			database: 'emufc'
		});
	}

	if (process.env.NODE_ENV === 'test') {
		return _mysql2.default.createConnection({
			host: 'localhost',
			user: 'almada',
			password: '123',
			database: 'emufc'
		});
	}

	if (process.env.NODE_ENV == 'production') {
		var url = process.env.CLEARDB_DATABASE_URL;
		var dbUrlParams = url.match(/mysql:\/\/(.*):(.*)@(.*)\/(.*)\?/);
		return _mysql2.default.createConnection({
			host: dbUrlParams[3],
			user: dbUrlParams[1],
			password: dbUrlParams[2],
			database: dbUrlParams[4]
		});
	}
};

exports.default = createDBConnection;