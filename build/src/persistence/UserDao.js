'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var UserDao = exports.UserDao = function () {
	function UserDao(connection) {
		_classCallCheck(this, UserDao);

		this._conn = connection;
	}

	_createClass(UserDao, [{
		key: 'findByEmail',
		value: function findByEmail(email, callback) {
			var query = 'SELECT * FROM tb_user WHERE email = ?';
			this._conn.query(query, [email], callback);
		}
	}, {
		key: 'updateToken',
		value: function updateToken(user, callback) {
			var query = 'UPDATE tb_user SET access_token = ? WHERE id = ?';
			this._conn.query(query, [user.access_token, user.id], callback);
		}
	}]);

	return UserDao;
}();