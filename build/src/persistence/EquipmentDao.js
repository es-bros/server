'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var EquipmentDao = exports.EquipmentDao = function () {
	function EquipmentDao(connection) {
		_classCallCheck(this, EquipmentDao);

		this._conn = connection;
	}

	_createClass(EquipmentDao, [{
		key: 'getAll',
		value: function getAll(callback) {
			var query = 'SELECT * FROM equipment';
			this._conn.query(query, callback);
		}
	}, {
		key: 'findById',
		value: function findById(id, callback) {
			var query = 'SELECT * FROM equipment WHERE id = ?';
			this._conn.query(query, [id], callback);
		}
	}, {
		key: 'findByPlaceId',
		value: function findByPlaceId(place_id, callback) {
			var query = 'SELECT * FROM equipment WHERE fk_place_id = ?';
			this._conn.query(query, [place_id], callback);
		}
	}, {
		key: 'findByResponsibleId',
		value: function findByResponsibleId(responsible_id, callback) {
			var query = 'SELECT * FROM equipment WHERE fk_responsible_id = ?';
			this._conn.query(query, [responsible_id], callback);
		}
	}, {
		key: 'save',
		value: function save(equipment, callback) {
			var query = 'INSERT INTO equipment SET ?';
			this._conn.query(query, equipment, callback);
		}
	}, {
		key: 'update',
		value: function update(equipment, callback) {
			var query = 'UPDATE equipment SET name = ?, description = ?, fk_place_id = ?, fk_responsible_id = ? WHERE id = ?';
			this._conn.query(query, [equipment.name, equipment.description, equipment.fk_place_id, equipment.fk_responsible_id, equipment.id], callback);
		}
	}, {
		key: 'remove',
		value: function remove(id, callback) {
			var query = 'DELETE FROM equipment WHERE id = ?';
			this._conn.query(query, [id], callback);
		}
	}]);

	return EquipmentDao;
}();