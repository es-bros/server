'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ResponsibleDao = exports.ResponsibleDao = function () {
	function ResponsibleDao(connection) {
		_classCallCheck(this, ResponsibleDao);

		this._conn = connection;
	}

	_createClass(ResponsibleDao, [{
		key: 'getAll',
		value: function getAll(callback) {
			var query = 'SELECT * FROM responsible';
			this._conn.query(query, [], callback);
		}
	}, {
		key: 'findById',
		value: function findById(id, callback) {
			var query = 'SELECT * FROM responsible WHERE id = ?';
			this._conn.query(query, [id], callback);
		}
	}, {
		key: 'save',
		value: function save(responsible, callback) {
			var query = 'INSERT INTO responsible SET ?';
			this._conn.query(query, responsible, callback);
		}
	}, {
		key: 'update',
		value: function update(responsible, callback) {
			var query = 'UPDATE responsible SET name = ?, email = ?, phone = ? WHERE id = ?';
			this._conn.query(query, [responsible.name, responsible.email, responsible.phone, responsible.id], callback);
		}
	}, {
		key: 'remove',
		value: function remove(id, callback) {
			var query = 'DELETE FROM responsible WHERE id = ?';
			this._conn.query(query, [id], callback);
		}
	}]);

	return ResponsibleDao;
}();