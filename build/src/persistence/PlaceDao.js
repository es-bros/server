'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var PlaceDao = exports.PlaceDao = function () {
	function PlaceDao(connection) {
		_classCallCheck(this, PlaceDao);

		this._conn = connection;
	}

	_createClass(PlaceDao, [{
		key: 'getAll',
		value: function getAll(callback) {
			var query = 'SELECT * FROM place';
			this._conn.query(query, [], callback);
		}
	}, {
		key: 'findById',
		value: function findById(id, callback) {
			var query = 'SELECT * FROM place WHERE id = ?';
			this._conn.query(query, [id], callback);
		}
	}, {
		key: 'save',
		value: function save(place, callback) {
			var query = 'INSERT INTO place SET ?';
			this._conn.query(query, place, callback);
		}
	}, {
		key: 'update',
		value: function update(place, callback) {
			var query = 'UPDATE place SET name = ?, latitude = ?, longitude = ? WHERE id = ?';
			this._conn.query(query, [place.name, place.latitude, place.longitude, place.id], callback);
		}
	}, {
		key: 'remove',
		value: function remove(id, callback) {
			var query = 'DELETE FROM place WHERE id = ?';
			this._conn.query(query, [id], callback);
		}
	}]);

	return PlaceDao;
}();