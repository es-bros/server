'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _equipments = require('../controllers/equipments');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();
var equipmentsController = new _equipments.EquipmentsController();

router.route('/').get(equipmentsController.getAll).post(equipmentsController.post);

router.route('/:id').get(equipmentsController.findById).put(equipmentsController.put).delete(equipmentsController.delete);

exports.default = router;