'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _places = require('../controllers/places');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();
var placesController = new _places.PlacesController();

router.route('/').get(placesController.getAll).post(placesController.post);

router.route('/:id').get(placesController.findById).put(placesController.put).delete(placesController.delete);

router.route('/:id/equipments').get(placesController.getEquipments);

exports.default = router;