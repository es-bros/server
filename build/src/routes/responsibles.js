'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _responsibles = require('../controllers/responsibles');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();
var responsiblesController = new _responsibles.ResponsiblesController();

router.route('/').get(responsiblesController.getAll).post(responsiblesController.post);

router.route('/:id').get(responsiblesController.findById).put(responsiblesController.put).delete(responsiblesController.delete);

router.route('/:id/equipments').get(responsiblesController.getEquipments);

exports.default = router;