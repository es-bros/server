'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _version = require('../controllers/version');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();
var versionController = new _version.VersionController();

router.route('/').get(versionController.get).put(versionController.increment);

exports.default = router;