'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _expressValidator = require('express-validator');

var _expressValidator2 = _interopRequireDefault(_expressValidator);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

var _helmet = require('helmet');

var _helmet2 = _interopRequireDefault(_helmet);

var _auth = require('./auth');

var _auth2 = _interopRequireDefault(_auth);

var _routes = require('./routes');

var _routes2 = _interopRequireDefault(_routes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();

var options = {
	customValidators: {
		isArray: function isArray(value) {
			return Array.isArray(value);
		}
	}
};

var apiKeys = ["API"];

var configureExpress = function configureExpress() {
	app.use((0, _helmet2.default)());
	app.use(_helmet2.default.noCache());

	app.use(_bodyParser2.default.json());
	_bodyParser2.default.urlencoded({ extended: true });

	app.use((0, _morgan2.default)('common'));

	app.use((0, _auth2.default)().initialize());

	// app.use('/api', function(req, res, next){
	//   var key = req.header['API_KEY'];

	//   	if (!key) return next(error(400, { error: 'API key required.'}));

	//  	if (!~apiKeys.indexOf(key)) return next(error(401, { error: 'Invalid API key'}));

	//  	req.key = key;
	// 	next();
	// });

	app.use((0, _expressValidator2.default)(options));

	app.use('/api', _routes2.default);

	app.use('/images', _express2.default.static(__dirname + '/images'));

	app.use(function (err, req, res, next) {
		res.status(err.status || 500);
		res.send({ error: err.message });
	});

	app.use(function (req, res) {
		res.status(404);
		res.send({ error: "Lame, can't find that" });
	});

	return app;
};

exports.default = configureExpress;