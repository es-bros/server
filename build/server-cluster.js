'use strict';

var _server = require('./server');

var _server2 = _interopRequireDefault(_server);

var _throng = require('throng');

var _throng2 = _interopRequireDefault(_throng);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var WORKERS = process.env.WEB_CONCURRENCY || 1;

(0, _throng2.default)({
  workers: WORKERS,
  lifetime: Infinity,
  start: _server2.default
});

// import cluster from 'cluster';
// import os  from 'os';

// var cpus = os.cpus();

// if(cluster.isMaster){
//   console.log('Thread master');

//   cpus.forEach(() => {
//       cluster.fork();
//   });

//   cluster.on('listening', worker => {
//     console.log('Cluster conectado: ' + worker.process.pid );
//   });

//   cluster.on('exit', worker => {
//     console.log('Cluster %d foi desconectado', worker.process.pid);
//     cluster.fork();
//   })

// } else {
//   console.log('\nThread Slave');
//   app(1);
// }