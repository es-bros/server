# emufc-service
- Webservice in NodeJS using **MySQL** as database, **ExpressJS** as server, **Morgan** as simple logger.

### Models:
- Places: [name, latitude, longitude]
- Equipments: [name, description, fk_responsible_id(responsible), fk_place_id(place)]
- Responsibles: [name, email, phone]
- User: [username, email, password, created_at, access_token]
- Version: [current, updated_at]

# NodeJS
- **NodeJS Version 6.10.3**
- **NPM Version 3.10.10**