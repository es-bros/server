import app from './server';
import throng from 'throng';

const WORKERS = process.env.WEB_CONCURRENCY || 1;

throng({
  workers: WORKERS,
  lifetime: Infinity,
  start: app
});

// import cluster from 'cluster';
// import os  from 'os';

// var cpus = os.cpus();

// if(cluster.isMaster){
//   console.log('Thread master');

//   cpus.forEach(() => {
//       cluster.fork();
//   });

//   cluster.on('listening', worker => {
//     console.log('Cluster conectado: ' + worker.process.pid );
//   });

//   cluster.on('exit', worker => {
//     console.log('Cluster %d foi desconectado', worker.process.pid);
//     cluster.fork();
//   })

// } else {
//   console.log('\nThread Slave');
//   app(1);
// }
