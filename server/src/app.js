import express from 'express';
import expressValidator from 'express-validator';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import helmet from 'helmet';

import auth from './auth';
import routes from './routes';

const app = express();

const options = {
	customValidators: {
    	isArray: function(value) {
        	return Array.isArray(value);
    	}
 	}
}

const apiKeys = ["API"];

const configureExpress = () => {
	app.use(helmet());
	app.use(helmet.noCache());

	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({extended: true}));

	app.use(morgan('common'));

	app.use(auth().initialize());

	// app.use('/api', function(req, res, next){
	//   var key = req.header['API_KEY'];

	//   	if (!key) return next(error(400, { error: 'API key required.'}));

	//  	if (!~apiKeys.indexOf(key)) return next(error(401, { error: 'Invalid API key'}));

	//  	req.key = key;
	// 	next();
	// });

	app.use(expressValidator(options));

	app.use('/api', routes);

	app.use('/images', express.static(__dirname + '/images'));


	app.use(function(err, req, res, next){
		res.status(err.status || 500);
		res.send({ error: err.message });
	});

	app.use(function(req, res){
		res.status(404);
		res.send({ error: "Lame, can't find that" });
	});

	return app;
}

export default configureExpress;
