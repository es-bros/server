import express from 'express';
import { PlacesController } from '../controllers/places';

const router = express.Router();
const placesController = new PlacesController();

router.route('/')
	.get(placesController.getAll)
	.post(placesController.post);

router.route('/:id')
	.get(placesController.findById)
	.put(placesController.put)
	.delete(placesController.delete);

router.route('/:id/equipments')
	.get(placesController.getEquipments);

export default router;