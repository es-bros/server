import express from 'express';
import { ResponsiblesController } from '../controllers/responsibles';

const router = express.Router();
const responsiblesController = new ResponsiblesController();

router.route('/')
	.get(responsiblesController.getAll)
	.post(responsiblesController.post);

router.route('/:id')
	.get(responsiblesController.findById)
	.put(responsiblesController.put)
	.delete(responsiblesController.delete);

router.route('/:id/equipments')
	.get(responsiblesController.getEquipments);

export default router;