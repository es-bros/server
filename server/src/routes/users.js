import express from 'express';
import { UsersController } from '../controllers/users';
import auth from '../auth';

const router = express.Router();
const usersController = new UsersController();

router.route('/login')
	.get(auth().authenticate(), usersController.get)
	.post(usersController.login);

export default router;