import express from 'express';

import equipmentsRoute from './equipments';
import placesRoute from './places';
import employeesRoute from './responsibles';
import usersRoute from './users';
import mobileRoute from './mobile';
import versionRoute from './version';

import json from '../../../data-from-last-version.json';

const router = express.Router();

router.use('/equipments', equipmentsRoute);
router.use('/places', placesRoute);
router.use('/responsibles', employeesRoute);
router.use('/mobile', mobileRoute);
router.use('/version', versionRoute);
router.use(usersRoute);

router.get('/test', (req, res) => res.send(json));

router.get('/', (req, res) => res.send({msg: 'Routes', defaultUrls: ['/version', '/mobile', '/equipments', '/places', '/responsibles', '/token']}));

export default router;