import express from 'express';
import { VersionController } from '../controllers/version';

const router = express.Router();
const versionController = new VersionController();

router.route('/')
	.get(versionController.get)
	.put(versionController.increment);

export default router;