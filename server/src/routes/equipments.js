import express from 'express';
import { EquipmentsController } from '../controllers/equipments';

const router = express.Router();
const equipmentsController = new EquipmentsController();

router.route('/')
	.get(equipmentsController.getAll)
	.post(equipmentsController.post);

router.route('/:id')
	.get(equipmentsController.findById)
	.put(equipmentsController.put)
	.delete(equipmentsController.delete);

export default router;