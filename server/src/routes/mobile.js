import express from 'express';
import { MobileController } from '../controllers/mobile';

const router = express.Router();
const mobileController = new MobileController();

router.route('/')
	.get(mobileController.get)

export default router;