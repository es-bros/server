import async from 'async';

import mysql from '../config/database';
import {VersionDao} from '../persistence/VersionDao';
import {EquipmentDao} from '../persistence/EquipmentDao';
import {PlaceDao} from '../persistence/PlaceDao';
import {ResponsibleDao} from '../persistence/ResponsibleDao';

export class MobileController {

	get(req, res) {

		const clientVersion = parseInt(req.query.version)

		const connection = mysql();
		const versionDao = new VersionDao(connection);

		async.waterfall(
			[
				function(callback) {
					versionDao.get((err, rows) => {
						const databaseVersion = rows[0].current;
						callback(err, databaseVersion);
					});
				},
				function(databaseVersion, callback) {
					clientVersion < databaseVersion ? callback(null, databaseVersion) : callback({msg: "Your database is up to date!"})
				},
				function(databaseVersion, callback) {
					_getAllDatabase(databaseVersion, connection, callback);
				}
			], function(err, results) {
				if(err) {
					res.status(400).send(err);
				} else {
					res.send(results);
				}

				return connection.end();
			}
		);
	}
	
	_getAllDatabase(databaseVersion, connection, cb) {

		const equipmentDao = new EquipmentDao(connection);
		const placeDao = new PlaceDao(connection);
		const responsibleDao = new ResponsibleDao(connection);
		
		async.parallel({
		    places: function(callback) {
		        placeDao.getAll((err, places) => {
		        	callback(err, places);
		        });
		    },
		    equipments: function(callback) {
		        equipmentDao.getAll((err, equipments) => {
		        	callback(err, equipments);
		        });
		    },
		    responsibles: function(callback) {
		        responsibleDao.getAll((err, responsibles) => {
		        	callback(err, responsibles);
		        });
		    }
		},
		function(err, results) {
			if(err)
				return cb(err);

			results.version = databaseVersion;
			return cb(null, results);
		});
	}
}