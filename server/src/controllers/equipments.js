import util from 'util';
import mysql from '../config/database';
import {EquipmentDao} from '../persistence/EquipmentDao';

export class EquipmentsController {

	getAll(req, res) {
		const pool = mysql();
		pool.getConnection((err, connection) => {
			if(err)
				return res.status(400).send(err);

			const equipmentDao = new EquipmentDao(connection);

			equipmentDao.getAll((err, rows) => {
				connection.release();

				if(err)
					return res.status(400).send(err);

				return res.send(rows);
			});
		});
	}

	findById(req, res) {
		req.checkParams('id', 'Id is empty').notEmpty()
			.withMessage('Must be an integer').isInt();

		req.getValidationResult().then(result => {
			if(!result.isEmpty())
				return res.status(400).send('There have been validation errors: ' + util.inspect(result.array()));
			
			const pool = mysql();
			pool.getConnection((err, connection) => {
				if(err)
					return res.status(400).send(err);

				const equipmentDao = new EquipmentDao(connection);
				const equipmentId = req.params.id;

				equipmentDao.findById(equipmentId, (err, rows) => {
					connection.release();

					if(err)
						return res.status(400).send(err);

					if(rows.length == 0)
						return res.sendStatus(404);

					res.send(rows[0]);
				});
			});
		});
	}

	post(req, res) {
		req.checkBody('name', 'Name is empty').notEmpty();
		req.checkBody('description', 'Description is empty').notEmpty();
		req.checkBody('fk_place_id', 'Place ID is empty').notEmpty().withMessage('Must be an integer').isInt();
		req.checkBody('fk_responsible_id', 'Responsible ID is empty').notEmpty().withMessage('Must be an integer').isInt();

		req.getValidationResult().then(result => {
			if(!result.isEmpty())
				return res.status(400).send('There have been validation errors: ' + util.inspect(result.array()));
			
			req.sanitizeBody('name').trim();
			req.sanitizeBody('description').trim();

			const pool = mysql();
			pool.getConnection((err, connection) => {
				if(err)
					return res.status(400).send(err);

				const equipmentDao = new EquipmentDao(connection);
				let equipment = req.body;

				equipmentDao.save(equipment, (err, row) => {
					connection.release();

					if(err)
						return res.status(400).send(err);

					equipment.id = row.insertId;

						const response = {
							'equipment': equipment,
							links: [
								{
									href: req.originalUrl + equipment.id,
									rel:"read",
									method:"GET"
								},
								{
									href: req.originalUrl + equipment.id,
									rel:"update",
									method:"PUT"
								},
								{
									href: req.originalUrl + equipment.id,
									rel:"delete",
									method:"DELETE"
								}
							]
						}

						res.location(req.originalUrl + equipment.id);
						res.status(201).send(response);
				});
			});
		});
	}

	put(req, res) {
		req.checkParams('id', 'Id is empty').notEmpty()
			.withMessage('Must be an integer').isInt();

		req.getValidationResult().then(result => {
			if(!result.isEmpty())
				return res.status(400).send('There have been validation errors: ' + util.inspect(result.array()));
			
			const pool = mysql();
			pool.getConnection((err, connection) => {
				if(err)
					return res.status(400).send(err);

				const equipmentDao = new EquipmentDao(connection);

				let equipment = req.body;
				equipment.id = req.params.id;

				equipmentDao.update(equipment, (err, row) => {
					connection.release()

					if(err)
						return res.status(400).send(err);

					if(row.affectedRows == 0)
						return res.sendStatus(404);

					res.sendStatus(200);
				});
			});
		});
	}

	delete(req, res) {
		req.checkParams('id', 'Id is empty').notEmpty()
			.withMessage('Must be an integer').isInt();

		req.getValidationResult().then(result => {
			if(!result.isEmpty())
				return res.status(400).send('There have been validation errors: ' + util.inspect(result.array()));

			const pool = mysql();
			pool.getConnection((err, connection) => {
				if(err)
					return res.status(400).send(err);

				const equipmentDao = new EquipmentDao(connection);
				const equipmentId = req.params.id;

				equipmentDao.remove(equipmentId, (err, result) => {
					connection.release();
					
					if(err)
						return res.status(400).send(err)

					res.sendStatus(204);
				});
			});
		});
	}
}