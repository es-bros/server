import util from 'util';
import mysql from '../config/database';
import {PlaceDao} from '../persistence/PlaceDao';
import {EquipmentDao} from '../persistence/EquipmentDao';

export class PlacesController {

	getAll(req, res) {
		const pool = mysql();
		pool.getConnection((err, connection) => {
			if(err)
				return res.status(400).send(err);

			const placeDao = new PlaceDao(connection);

			placeDao.getAll((err, rows) => {
				connection.release();

				if(err)
					return res.status(400).send(err)

				res.send(rows);
			});
		});
	}

	findById(req, res) {
		req.checkParams('id', 'Id is empty').notEmpty()
			.withMessage('Must be an integer').isInt();

		req.getValidationResult().then(result => {
			if(!result.isEmpty())
				return res.status(400).send('There have been validation errors: ' + util.inspect(result.array()));
		
			const pool = mysql();
			pool.getConnection((err, connection) => {
				if(err)
					return res.status(400).send(err);

				const placeDao = new PlaceDao(connection);
				const placeId = req.params.id;

				placeDao.findById(placeId, (err, rows) => {
					connection.release();

					if(err)
						return res.sendStatus(400);

					if(rows.length == 0)
						return res.sendStatus(404);
						
					res.send(rows[0]);
				});
			});
		});
	}

	getEquipments(req, res) {
		req.checkParams('id', 'Id is empty').notEmpty()
			.withMessage('Must be an integer').isInt();

		req.getValidationResult().then(result => {
			if(!result.isEmpty())
				return res.status(400).send('There have been validation errors: ' + util.inspect(result.array()));

			const pool = mysql();
			pool.getConnection((err, connection) => {
				if(err)
					return res.status(400).send(err);

				const equipmentDao = new EquipmentDao(connection);

				const placeId = req.params.id;

				equipmentDao.findByPlaceId(placeId, (err, rows) => {
					if(err)
						return res.status(400).send(err);

					res.send(rows);
				});
			});
		});
	}

	post(req, res) {
		req.checkBody('name', 'Name is empty').notEmpty();
		req.checkBody('latitude', 'Latitude is empty').notEmpty();
		req.checkBody('longitude', 'Longitude is empty').notEmpty();

		req.getValidationResult().then(result => {
			if(!result.isEmpty())
				return res.status(400).send('There have been validation errors: ' + util.inspect(result.array()));

			req.sanitizeBody('name').trim();
			req.sanitizeBody('description').trim();

			const pool = mysql();
			pool.getConnection((err, connection) => {
				if(err)
					return res.status(400).send(err);

				let place = req.body;
				const placeDao = new PlaceDao(connection);

				placeDao.save(place, (err, row) => {
					connection.release();
					if(err)
						return res.status(400).send(err)

					place.id = row.insertId;

					const response = {
						'place': place,
						links: [
							{
								href: req.originalUrl + place.id,
								rel:"read",
								method:"GET"
							},
							{
								href: req.originalUrl + place.id,
								rel:"update",
								method:"PUT"
							},
							{
								href: req.originalUrl + place.id,
								rel:"delete",
								method:"DELETE"
							}
						]
					}

					res.location(req.originalUrl + place.id);
					res.status(201).send(response);
				});
			});
		});
	}

	put(req, res) {
		req.checkParams('id', 'Id is empty').notEmpty()
			.withMessage('Must be an integer').isInt();

		req.getValidationResult().then(result => {
			if(!result.isEmpty())
				return res.status(400).send('There have been validation errors: ' + util.inspect(result.array()));

			const pool = mysql();
			pool.getConnection((err, connection) => {
				if(err)
					return res.status(400).send(err);

				const placeDao = new PlaceDao(connection);

				let place = req.body
				place.id = req.params.id;

				placeDao.update(place, (err, row) => {
					connection.release();
					if(err)
						return res.status(400).send(err)

					if(row.affectedRows == 0)
						return res.sendStatus(404);

					res.sendStatus(200);
				});
			});
		});
	}

	delete(req, res) {
		req.checkParams('id', 'Id is empty').notEmpty()
			.withMessage('Must be an integer').isInt();

		req.getValidationResult().then(result => {
			if(!result.isEmpty())
				return res.status(400).send('There have been validation errors: ' + util.inspect(result.array()));

			const pool = mysql();
			pool.getConnection((err, connection) => {
				if(err)
					return res.status(400).send(err);

				const placeDao = new PlaceDao(connection);
				const placeId = req.params.id;

				placeDao.remove(placeId, (err, row) => {
					connection.release();

					if(err)
						return res.status(400).send(err);

					res.sendStatus(204);
				});
			});
		});
	}
}