import async from 'async';

import mysql from '../config/database';
import {VersionDao} from '../persistence/VersionDao';

export class VersionController {

	get(req, res) {
		const pool = mysql();
		pool.getConnection((err, connection) => {
			if(err)
					return res.status(400).send(err);
			
			const versionDao = new VersionDao(connection);

			versionDao.get((err, rows) => {
				connection.release();

				if(err || rows.length == 0)
					return res.status(400).send(err);

				res.send(rows[0]);
			});
		});
	}

	increment(req, res) {
		pool.getConnection((err, connection) => {
			if(err)
					return res.status(400).send(err);

			const versionDao = new VersionDao(connection);

			//utilizar transaction
			async.waterfall(
				[
					function(callback) {
						versionDao.get((err, rows) => {
							const currentVersion = rows[0].current;
							callback(err, currentVersion);
						});
					},
					function(currentVersion, callback) {
						const newVersion = currentVersion++;
						versionDao.update(newVersion, new Date(), (err, rows) => {
							callback(err, newVersion)
						});
					}
				], function(err, newVersion) {
					connection.release();
					if(err)
						return res.status(400).send(err);

					res.send({ current : newVersion });
				}
			);
		});
	}
}