import util from 'util';
import jwt from 'jsonwebtoken';
import async from 'async';

import mysql from '../config/database';
import cfg from '../config/config';
import {UserDao} from '../persistence/UserDao';

export class UsersController {

	get(req, res) {
		res.send({msg: "Token autenticado com sucesso!"});
	}

	login(req, res) {
		req.checkBody('email', 'Invalid email').notEmpty().isEmail();
		req.checkBody('password', 'Invalid password').notEmpty();

		req.getValidationResult().then(result => {
			if(!result.isEmpty())
				return res.status(400).send('There have been validation errors: ' + util.inspect(result.array()));

			const pool = mysql();
			pool.getConnection((err, connection) => {
				if(err)
					return res.status(400).send(err);

				let	userDao = new UserDao(connection);

				async.waterfall(
					[
						function(callback) {
							userDao.findByEmail(req.body.email, (err, rows) => {
								if(rows.length == 0) {
									return callback({msg : 'Usuário inválido.'});
								}
								
								let user = rows[0];
								callback(err, user);
							});
						},
						function(user, callback) {
							if(user.password == req.body.password) {
								const payload = {email: user.email};
								const token = jwt.sign(payload, cfg.security.jwtSecret, {expiresIn: '7d'});
								user.access_token = token;
								userDao.updateToken(user, (err, row) => {
									callback(err, token);
								});
							} else {
								const err = { msg : "Senha inválida."}
								callback(err)
							}
						}
					], function(err, token) {
							connection.release();
							if(err)
								return res.status(400).send(err);

							res.send({token: token});
						}
				);
			});
		});
	}
}
