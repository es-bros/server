import util from 'util';
import mysql from '../config/database';
import {ResponsibleDao} from '../persistence/ResponsibleDao';
import {EquipmentDao} from '../persistence/EquipmentDao';

export class ResponsiblesController {

	getAll(req, res) {
		const pool = mysql();
		pool.getConnection((err, connection) => {
			if(err)
				return res.status(400).send(err);

			const responsibleDao = new ResponsibleDao(connection);

			responsibleDao.getAll((err, rows) => {
				connection.release();

				if(err)
					return res.status(400).send(err);

				res.send(rows);
			});
		});
	}

	findById(req, res) {
		req.checkParams('id', 'Id is empty').notEmpty()
			.withMessage('Must be an integer').isInt();

		req.getValidationResult().then(result => {
			if(!result.isEmpty())
				return res.status(400).send('There have been validation errors: ' + util.inspect(result.array()));

			const pool = mysql();
			pool.getConnection((err, connection) => {
				if(err)
					return res.status(400).send(err);

				const responsibleDao = new ResponsibleDao(connection);
				const responsibleId = req.params.id;

				responsibleDao.findById(responsibleId, (err, row) => {
					connection.release();

					if(err)
						return res.status(400).send(err);

					if(row.length == 0)
						return res.sendStatus(404);

					res.send(row[0]);
				});
			});
		});
	}

	post(req, res) {
		req.checkBody('name', 'Name is empty').notEmpty();

		req.checkBody('email', 'E-mail is empty').notEmpty()
			.withMessage('Valid e-mail required').isEmail();

		req.checkBody('phone', 'Phone is empty').notEmpty();

		req.getValidationResult().then(result => {
			if(!result.isEmpty())
				return res.status(400).send('There have been validation errors: ' + util.inspect(result.array()));

			req.sanitizeBody('name').trim();
			req.sanitizeBody('email').trim();
			req.sanitizeBody('phone').trim();

			const pool = mysql();
			pool.getConnection((err, connection) => {
				if(err)
					return res.status(400).send(err);
				
				const responsibleDao = new ResponsibleDao(connection);

				let responsible = req.body;

				responsibleDao.save(responsible, (err, row) => {
					connection.release();

					if(err)
						return res.status(400).send(err);

					responsible.id = row.insertId;

					const response = {
						'responsible': responsible,
						links: [
							{
								href: req.originalUrl + responsible.id,
								rel:"read",
								method:"GET"
							},
							{
								href: req.originalUrl + responsible.id,
								rel:"update",
								method:"PUT"
							},
							{
								href: req.originalUrl + responsible.id,
								rel:"delete",
								method:"DELETE"
							}
						]
					}

					res.location(req.originalUrl + responsible.id);
					res.status(201).send(response);
				});
			});
		});
	}

	put(req, res) {
		req.checkParams('id', 'Id is empty').notEmpty()
			.withMessage('Must be an integer').isInt();

		req.getValidationResult().then(result => {
			if(!result.isEmpty())
				return res.status(400).send('There have been validation errors: ' + util.inspect(result.array()));

			const pool = mysql();
			pool.getConnection((err, connection) => {
				if(err)
					return res.status(400).send(err);

				const responsibleDao = new ResponsibleDao(connection);

				let responsible = req.body
				responsible.id = req.params.id;

				responsibleDao.update(responsible, (err, row) => {
					connection.release();

					if(err)
						return res.status(400).send(err);

					if(row.affectedRows == 0)
						return res.sendStatus(404);

					res.sendStatus(200);
				});
			});
		});
	}

	delete(req, res) {
		req.checkParams('id', 'Id is empty').notEmpty()
			.withMessage('Must be an integer').isInt();

		req.getValidationResult().then(result => {
			if(!result.isEmpty())
				return res.status(400).send('There have been validation errors: ' + util.inspect(result.array()));

			const pool = mysql();
			pool.getConnection((err, connection) => {
				if(err)
					return res.status(400).send(err);

				const responsibleDao = new ResponsibleDao(connection);
				const responsibleId = req.params.id;

				responsibleDao.remove(responsibleId, (err, rows) => {
					connection.release();
					if(err)
						return res.status(400).send(err);

					res.sendStatus(204);
				});
			});
		});
	}

	getEquipments(req, res) {
		req.checkParams('id', 'Id is empty').notEmpty()
			.withMessage('Must be an integer').isInt();

		req.getValidationResult().then(result => {
			if(!result.isEmpty())
				return res.status(400).send('There have been validation errors: ' + util.inspect(result.array()));

			const pool = mysql();
			pool.getConnection((err, connection) => {
				if(err)
					return res.status(400).send(err);
				
				const equipmentDao = new EquipmentDao(connection);
				const responsibleId = req.params.id;

				equipmentDao.findByResponsibleId(responsibleId, (err, rows) => {
					connection.release();
					
					if(err)
						return res.status(400).send(err);

					res.send(rows);
				});
			});
		});
	}
}
