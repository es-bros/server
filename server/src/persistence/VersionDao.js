export class VersionDao {

	constructor(connection) {
		this._conn = connection;
	}

	get(callback) {
		const query = 'SELECT current FROM version WHERE id = 1';
		this._conn.query(query, [], callback);
	}

	update(current, updatedAt, callback) {
		const query = 'UPDATE version SET current = ?, updated_at = ? WHERE id = 1';
		this._conn.query(query, [current, updatedAt], callback);
	}

}