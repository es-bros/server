export class PlaceDao {

	constructor(connection) {
		this._conn = connection;
	}

	getAll(callback) {
		const query = 'SELECT * FROM place';
		this._conn.query(query, [], callback);
	}

	findById(id, callback) {
		const query = 'SELECT * FROM place WHERE id = ?';
		this._conn.query(query, [id], callback);
	}

	save(place, callback) {
		const query = 'INSERT INTO place SET ?';
		this._conn.query(query, place, callback);
	}

	update(place, callback) {
		const query = 'UPDATE place SET name = ?, latitude = ?, longitude = ? WHERE id = ?';
		this._conn.query(query, [place.name, place.latitude, place.longitude, place.id], callback);
	}

	remove(id, callback) {
		const query = 'DELETE FROM place WHERE id = ?'
		this._conn.query(query, [id], callback);
	}

}