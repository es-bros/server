export class EquipmentDao {

	constructor(connection) {
		this._conn = connection;
	}

	getAll(callback) {
		const query = 'SELECT * FROM equipment';
		this._conn.query(query, [], callback);
	}

	findById(id, callback) {
		const query = 'SELECT * FROM equipment WHERE id = ?';
		this._conn.query(query, [id], callback);
	}

	findByPlaceId(place_id, callback) {
		const query = 'SELECT * FROM equipment WHERE fk_place_id = ?';
		this._conn.query(query, [place_id], callback);
	}

	findByResponsibleId(responsible_id, callback) {
		const query = 'SELECT * FROM equipment WHERE fk_responsible_id = ?';
		this._conn.query(query, [responsible_id], callback);
	}

	save(equipment, callback) {
		const query = 'INSERT INTO equipment SET ?';
		this._conn.query(query, equipment, callback);
	}

	update(equipment, callback) {
		const query = 'UPDATE equipment SET name = ?, description = ?, fk_place_id = ?, fk_responsible_id = ? WHERE id = ?';
		this._conn.query(query, [equipment.name, equipment.description, equipment.fk_place_id, equipment.fk_responsible_id, equipment.id], callback);
	}

	remove(id, callback) {
		const query = 'DELETE FROM equipment WHERE id = ?'
		this._conn.query(query, [id], callback);
	}

}