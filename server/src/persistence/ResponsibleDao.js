export class ResponsibleDao {

	constructor(connection) {
		this._conn = connection;
	}

	getAll(callback) {
		const query = 'SELECT * FROM responsible';
		this._conn.query(query, [], callback);
	}

	findById(id, callback) {
		const query = 'SELECT * FROM responsible WHERE id = ?';
		this._conn.query(query, [id], callback);
	}

	save(responsible, callback) {
		const query = 'INSERT INTO responsible SET ?';
		this._conn.query(query, responsible, callback);
	}

	update(responsible, callback) {
		const query = 'UPDATE responsible SET name = ?, email = ?, phone = ? WHERE id = ?';
		this._conn.query(query, [responsible.name, responsible.email, responsible.phone, responsible.id], callback);
	}

	remove(id, callback) {
		const query = 'DELETE FROM responsible WHERE id = ?'
		this._conn.query(query, [id], callback);
	}

}