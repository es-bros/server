export class UserDao {

	constructor(connection) {
		this._conn = connection;
	}

	findByEmail(email, callback) {
		const query = 'SELECT * FROM tb_user WHERE email = ?';
		this._conn.query(query, [email], callback);
	}

	updateToken(user, callback) {
		const query = 'UPDATE tb_user SET access_token = ? WHERE id = ?';
		this._conn.query(query, [user.access_token, user.id], callback);
	}

}