import passport from 'passport';
import passportJWT from 'passport-jwt';
import jwt from 'jsonwebtoken';

import { UserDao } from './persistence/UserDao';
import cfg from './config/config';
import mysql from './config/database';

var ExtractJwt = passportJWT.ExtractJwt;
var Strategy = passportJWT.Strategy;

var params = {
	secretOrKey: cfg.security.jwtSecret,
	jwtFromRequest: ExtractJwt.fromAuthHeader()
}

export default () => {

	var strategy = new Strategy(params, (payload, done) => {

		const connection = mysql();
		const userDao = new UserDao(connection);

		userDao.findByEmail(payload.email, (err, rows) => {
			if(err || rows.length == 0)
				return done(new Error("User not found"), false);

			const user = rows[0];

			if(user) {
				const userToken = jwt.decode(user.access_token);
				
				if(userToken.email == payload.email && userToken.iat == payload.iat && userToken.exp == payload.exp) {
					return done(null, user);
				}
			}

			return done(new Error("User or token invalid"), false);
		});

		return connection.end();
	});

	passport.use(strategy);

	return {
		initialize : () => {
			return passport.initialize();
		},
		authenticate : () => {
			return passport.authenticate("jwt", cfg.security.jwtSession);
		}
	};

};