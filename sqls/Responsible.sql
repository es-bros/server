CREATE TABLE responsible (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL,
	email VARCHAR(70) NOT NULL,
	phone VARCHAR(20) NOT NULL,

	CONSTRAINT responsible_PK PRIMARY KEY (id)	
);

CREATE INDEX responsible_IDX_id ON responsible (id) USING HASH;