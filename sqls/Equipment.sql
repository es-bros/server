CREATE TABLE equipment (
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(120) NOT NULL,
	description TEXT NOT NULL,
	fk_place_id INT,
	fk_responsible_id INT,

	CONSTRAINT equipment_PK PRIMARY KEY (id),
	CONSTRAINT equipment_FK_place_id FOREIGN KEY (fk_place_id) REFERENCES place(id) ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT equipment_FK_responsible_id FOREIGN KEY (fk_responsible_id) REFERENCES responsible(id) ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE INDEX equipment_IDX_place_id ON equipment (fk_place_id) USING HASH;
CREATE INDEX equipment_IDX_responsible_id ON equipment (fk_responsible_id) USING HASH;