INSERT INTO version (id, current) VALUES
(1, 0);

INSERT INTO responsible (id, name, email, phone) VALUES
(1, "Antônio Gomes Souza Filho", "agsf@fisica.ufc.br", "(85) 9 9999-9999");

INSERT INTO place (id, name, latitude, longitude) VALUES
(1, "Departamento de Física - Bloco 929", -3.747014, -38.576372),
(2, "Departamento de Física - Bloco 929", -3.747052, -38.576600);

INSERT INTO equipment (name, description, fk_place_id, fk_responsible_id) VALUES
("Microscópio confocal - LM 710 Zeiss", "Possui 6 linhas de lasers. Gera imagens tridimensionais com alto contraste e
especificidade de sinal.", 1, 1),
("Witec alpha300", "Imagens Raman confocal\nResolução nominal de 200-300nm\nResolução 3D excepcional\nImagens Raman ultra-rápidas (0.76ms)", 1, 1),
("MEV Inspect S50 - FEI", "Resolução nominal de 3nm\nAnálise de superfícies de materiais em alto e baixo vácuo\nEDS e Litografia de elétrons", 2, 1),
("MEV - Quanta 450 FEG - FEI", "Resolução nominal de 1nm\nAnálise de superfícies de materiais em alto, baixo vácuo e pressão variável\nEBSD e EDS em campo simples ou amplo", 2, 1),
("Leitor de Placas Enspire – PerkinElmer", "Leituras em diferentes placas e comprimentos de onda\nAnálises rápidas e precisas em alta sensibilidade", 1, 1),
("Metalizadora QT150 ES - Quorum", "Cobertura rápida e precisa com Au, Cr, Ir e outros\nCobertura com carbono", 2, 1),
("Ultramicrótomo UC7 - Leica", "Realiza cortes ultrafinos em diferentes espessuras (até 20nm)\nDisplay digital\nFacilidade de manipulação", 2, 1),
("Micrótomo Rotativo - RM2265 - Leica", "Realiza cortes finos em diferentes espessuras (até 500nm)\nCorte automatizado de materiais de diferentes naturezas", 2, 1);

INSERT INTO tb_user (username, email, password, access_token) VALUES
('icarotavares', 'icarotavares@live.com', '123456', 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImljYXJvdGF2YXJlc0BsaXZlLmNvbSIsImlhdCI6MTQ5NTkzNDEwNX0.kM-EjNNXa6RkQxvgeW1fl5CcNulg62lbG57dK9Q_AwE'),
('almada', 'almada@gmail.com', '123456', 'JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImljYXJvdGF2YXJlc0BsaXZlLmNvbSIsImlhdCI6MTQ5NTkzNDEwNX0.kM-EjNNXa6RkQxvgeW1fl5CcNulg62lbG57dK9Q_AwE');