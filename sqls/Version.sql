CREATE TABLE version (
  id INT,
  current INT,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP 
                     ON UPDATE CURRENT_TIMESTAMP,
                     
  CONSTRAINT version_PK PRIMARY KEY (id)
);